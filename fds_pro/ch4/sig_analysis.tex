
\chapter{Analyse et traitement des signaux sismiques}

\section{Introduction}

Les ondes sismiques sont des ondes élastiques qui peuvent traverser un milieu en le modifiant selon l'intensité du séisme. Le caractère hétérogène de la croûte terrestre influence fortement la propagation de ces ondes et limite le degré de compréhension de la répartition temporelle de l’énergie que présentent les sismogrammes régionaux. D'autre part, les capteurs sismiques enregistrent les vibrations du sol sans prendre en compte la cause du phénomène; ce qui peut être \`a la base de fausses alertes sismiques sur le système. Après l'acquisition et la transmission des signaux \`a travers le réseau sismique, il vient maintenant les besoins des géophysiciens ou des sismologues d'interpréter et d'analyser les données recueillies. L'analyse et l'interprétation des signaux sismiques permet d'étudier:
\begin{itemize}
	\item La structure interne de la terre
	\item La polarisation des signaux sismiques
	\item Les paramètres sources des séismes
\end{itemize}
Ce chapitre représente la suite d'analyse que nous effectuons depuis le chapitre précédent. Nous n'allons pas faire une étude approfondie des signaux sismiques, mais nous présenterons les processus de filtrage et de séparation de phases sismiques puis nous présenterons une méthode de classification des signaux dans un système de surveillance sismique.


\section{Traitement des flux de données sismiques}

%Le traitement des données sismiques est un champs d’application pour les traitement de signaux. 
Le processus de traitement des signaux sismiques peut être divisé en cinq grandes phases\cite[p.29]{asismiq_key}
\begin{enumerate}
	\item Première phase: Pré-traitement (harmonisation des enregistrements, amélioration de la résolution, extraction de ondes réfléchies, filtrage des ondes indésirables)
	\item Deuxième phase: Calcul des corrections statiques et dynamiques, détermination des champs de vitesse ;
	\item Troisième phase: Sommation en couverture multiple ;
    \item  Quatrième phase: Migration et inversion ;
	\item Cinquième phase: Estimation des zones réservoirs.
\end{enumerate}

\subsection{Première phase: Pré-traitement}

Cette première phase comprend des opérations telles que l’édition, la récupération des amplitudes,
les filtrages en vitesse apparente, les déconvolutions et démultiplications.

\paragraph{L’édition}

L’édition désigne toutes les opérations qu’il faut faire sur les enregistrements avant tout traitement. L’édition comporte la mise des enregistrements terrain au format de traitement. L’édition comprend également la suppression des mauvaises traces, l’application de filtre en fréquence. 


\paragraph{Filtre en fréquence et en vitesse apparente}

Les traces sismiques peuvent avoir des contenus fréquentiels très variés. Si les bruits ont des fréquence différentes du signal utile, on peut les atténuer par un filtre en fréquence classique (passe-haut, passe-bas ou passe-bande). On peut également utiliser des filtres à deux dimensions (filtre en vitesse apparente) qui élimineront les bruits organisés. Un filtrage brutal consiste à mettre à zéro une partie des traces. Cette opération s’appelle un mute. 
\begin{figure}[h!]
	\centering
	\includegraphics[width=0.70\textwidth]{../ch4/data/ampl}
	\caption{Récupération des amplitudes. Source \cite[p.29]{asismiq_key}}
\end{figure}
\FloatBarrier

\subsection{Deuxième phase : corrections diverses et analyse de vitesse}

La deuxième phase consiste à déterminer les paramètres de la zone altérée, à appliquer des corrections statiques, à déterminer les analyses de vitesse et à appliquer des corrections dynamiques. Il existe tous un lot de programme automatiques calculant les corrections statiques et corrections statiques résiduelles. 

\subsection{Troisième phase : les sommations}

Cette phase aboutit à l’obtention d’une section sismique à deux ou trois dimensions. Elle correspond à la sommation des traces groupées en collection point milieu-commun. Cette sommation améliore le rapport signal sur bruit aléatoire d’un facteur $\sqrt{n}$ ou $n$ est l’ordre de la sommation. Presque tous les traitements que nous avons évoqués dans les deux premières phases doivent être impérativement faits avant la sommation. Certains traitements peuvent être faits après notamment tous les filtrages linéaires vis à vis de la sommation (filtre en fréquence), l’enlèvement des multiples et les filtres basées sur des séparations d’espaces.


\subsection{Quatrième phase: migration et inversion}

Cette phase comprend des traitements spécifiques tels que la migration, l’inversion des sections
sismiques pour les transformer en sections d’impédance acoustique. 

\paragraph{Migration}

La migration est une opération consistant à réarranger l’information sismique telle que les réflexions et les réfractions soient positionnées à leurs vraies positions. Originellement effectuée à la main par les interprétateurs, les migrations sont basées sur une intégration le long des courbes de diffraction (Migration de Kirchoff), effectuées par des algorithmes nombreux et variés utilisant soit des différences finies, soit des déphasages de champs d’ondes et/ou leurs équivalents dans les domaines duales.

\paragraph{Inversion}

L’inversion des données sismiques est basée sur la comparaison des données acquises avec des données synthétiques modélisées représentant des temps théoriques calculés à partir d’un modèle géologique donné à priori et d’un algorithme de lancé de rayons. Le but de l’inversion est de trouver les paramètres du modèle (limite de couches, et vitesse de couches) tels que les temps théoriques soient les plus proches possibles des temps mesurés (minimisation d’une fonction coût moindre carrés).

\subsection{Cinquième phase : estimation des zones réservoirs}

Une fois les données correctement traitées du point de vue du signal, l’image qui en ressort est donc le plus proche possible de la réalité terrain. L’étape finale consiste à faire une interprétation stratigraphique pour obtenir des informations sur les formes structurales, mais aussi sur la nature des dépôts, leurs environnements et les processus de sédimentation.

\subsection{Séparation d'ondes}

La compréhension des phénomènes complexes mis en jeu lors de la propagation du champ d’onde dans la croûte terrestre passe par une identification complète des différentes phases sismiques présentes sur les sismogrammes. Une phase sera complètement décrite et identifiée lorsqu'on lui attribue un temps d’arrivée, un contenu fréquentiel, un vecteur nombre d’onde (ou encore une direction et une vitesse apparente de propagation) ainsi qu’une énergie ou un poids par rapport à l’énergie globale contenue dans le sismogramme. Les méthodes de séparation d'ondes ou de phase sismiques peuvent être classées en deux catégories :

\begin{itemize}
	\item Les méthodes à gabarit;
	\item Les méthodes matricielles;
\end{itemize}
Les méthodes \`a gabarit (ou \`a masque) réalisent un changement de domaine permettant \`a des ondes qui n’etaient pas séparées dans le domaine temps-distance de le devenir dans le nouveau domaine. Un masque est alors appliqué aux données pour sélectionner les évènements intéressants puis les données filtrées sont repassées dans le domaine initial. Les méthodes matricielles sont basées sur la construction d’une matrice ou d’un cube de données qui est décomposée en deux ou plusieurs espaces. Les données de départ sont ensuite projetées sur ces différents espaces.


\subsection{Filtrage}

L'hétérogénéité des milieux qu'ils traversent engendre la complexité de l'étude des signaux sismiques\cite{mem_key}. Le traitement des signaux intervient donc en proposant des méthodes de représentation et de caractérisation permettant de faciliter interprétation de ces signaux. L'analyse standard des signaux sismiques comprend toutes les opération de prétraitement et de traitement de données pour l'interprétation des sismogrammes. Dans le domaine temporel, les processus importants sont la détection des signaux, le filtrage, la restitution, la simulation, la sélection de phase et l'analyse de la polarisation. Dans le domaine fréquentiel, les procédures principales sont la transformée fréquence-nombre (transformée $ f-k $) et l'analyse spectrale. Le filtrage, la séparation de phases sismiques et la classification des signaux sont nos principaux objectifs au cours de cette étude.

Le filtre f-k utilise la transformée fréquence-nombre d’ondes (transformée f-k) qui s'obtient en prenant le module de la double transformée de Fourier en temps et en distance de la section sismique initiale. f correspond aux fréquences temporelles et k aux fréquences spatiales aussi appelées nombres d’ondes. Ce filtre, appelé également "filtre en vitesse", est très souvent utilisé en séparation d’ondes sismique car le paramètre discriminant pour séparer les événements est leur vitesse (entité facilement interprétable). Le filtre basé sur la discrimination en vitesse apparente des arrivées cohérentes. Soit une fonction $r(t,x)$, par définition sa double transformée de Fourier est\footnote{Ces resultats ont été retrouvés en \cite[p.32]{mem_key}} 
\[
 TF2D{r(t,x)} = TFx{TFt[r(t,x)]} = R(nt, nx)
\]\cite[p.32]{mem_key}

\begin{itemize}
	\item $f_{t}$: fréquence temporelle; $\omega=2\pi f_{t}$ est la pulsation.
	\item $f_{x}$: fréquence spatiale; $k = 2\pi f_{x}$ est le nombre d'onde 
\end{itemize}

Cette transformation est particulièrement intéressante pour des fonctions représentant des signaux se propageant à vitesse $V$ constante et observés sur un réseau de capteurs régulièrement espacés car :
\[
	r(t,x)= g(t- x/V)
\]

\[
	TF[g(t)] = G(f)
\]

\[
	TF_{t}[g(t-x/V)] = G(f)e^{-2\pi ifx/V}
\]

\[
	TF_{X}[G(f)e^{-2\pi fk/V}] = G(f) \delta(k + f/V)
\]

dans le plan $f, k$ ou $\omega,k \delta(k + f/V)\Rightarrow droite k = f/V $

\[
	TF_{x}{TF_{t}[g(t-x/V)]} = G(f)\delta(k-\frac{f}{V})
\]\cite[p.32]{mem_key}

Dans le plan $\omega-k$, $\delta(k-\frac{f}{V})$ représente la droite $k=\frac{f}{V}$ passant par l'origine et de pente $V$. Ainsi une onde plane de vitesse $V$ dans le plan $[X,T]$, aura un motif au point $\frac{f}{k} = V$. Si cette onde possède une vitesse de groupe différente de la vitesse de phase, il y a donc dispersion. La vitesse de groupe est la vitesse de enveloppe des signaux et la vitesse de phase celle d’une arche à l’intérieur de l’enveloppe. Quand une onde est déphasée d’un capteur à l’autre, cela veut dire que:

\[
	TF_{X}[G(f)e^{-2\pi fk/V}.e^{-ik_{0}X}] = G(f)\delta(k-\frac{f}{V} + \frac{k_{0}}{2\pi})
\]

La double transformée permet de:
\begin{itemize}
	\item mettre en évidence les ondes planes ,
	\item estimer les vitesses apparentes les vitesses de groupes et de phase,
	\item filtrer les ondes de vitesse différentes,
	\item améliorer le rapport signal sur bruit. Ce filtrage sera réalisé par des filtres à gabarit de différents types selon l’application.
\end{itemize}

Il existe de nombreux filtres utilisés dans le traitement des signaux sismiques, on retrouve l'implementation des certains dans le projet ObsPy\footnote{ObsPy est un projet open-source dédié à fournir un Framework Python pour le traitement des données sismiques\cite{obs_key}}.

\section{Gestion d'événements sismiques}

L'approche conventionnelle pour la détection d'événements sismiques  et de la localisation consiste en quatre étapes:
\begin{enumerate}
	\item Détection du signal
	\item Identification de phase (P,S, etc)
	\item Association de phase, phases correspondant à de nombreuses stations
	\item Détection d'événement en utilisant l'association des phases
\end{enumerate}

Dans certains réseaux de surveillance sismique, la méthode de création d'événements consiste en une procédure automatique qui détermine en temps réel:
\begin{itemize}
	\item La localisation
	\item La magnitude
	\item Un shake-map
\end{itemize}

Après le résultat de la procédure automatique, le processus de validation se fait manuellement. Une fois validé, l'événement est stocké dans une base de données. Toutes les données et les événement sismiques sont considérés comme fiables seulement après une validation manuelle. Les résultats de la procédure manuelle et automatique sont stockes dans la base de données du centre de traitement et d'archivage. Dans la surveillance de la sismicité locale et régionale, de nombreux centre d'observation sismique utilisent des procédures automatiques de détection et de localisation d'événement. Cependant, dans la gestion automatique d'événement sismique, les événements sont parfois mal classés ou non-classés. Pour résoudre ce problème, il convient d'appliquer des méthodes fiables pour classer les données d'événements sismiques de manière automatique.

La classification automatique est nécessaire \`a la surveillance de risque naturel dans le cas ou la transmission d'information fiable et rapide est prioritaire aux autorités locales et les médias. De plus elle permet de maintenir le catalogue d'événement sismique.

\section{Classification des signaux par l'algorithme SVM}

Dans les systèmes de surveillance sismiques, la détection automatiques d'événements peut parfois données fausses alertes. Cela est du par le fait que les sismomètres enregistrent tout ce qui vibre le sol; une explosion, le passage d'une voiture ou l'effondrement d'un bâtiment. Pour traiter les données sismiques et obtenir un bonne classification automatique des événements sismiques, il faut avoir recours \`a des méthodes d'analyse sophistiquées. Dans cette section, nous pressentons une méthode de classification des événements sismiques en utilisant l'algorithme d'apprentissage automatique SVM(Support Vector Machine, en français, machine \`a vecteur de support ou machine \`a vaste marge). Cette méthode sera basée sur des particularités pressentes dans la distribution énergétiques des signaux enregistrés par les sismomètres. Nous ramenons ces particularités \`a un problème de classification et ensuite nous appliquons l'algorithme SVM sur les échantillons de données recueillies. 


\subsection{Presentation de l'agorithme}

On se place dans le plan et l’on dispose de deux catégories d'objet: les ronds rouges et les carrés bleus (Figure \ref{svm_m}), chacune occupant une région différente du plan. Cependant, la frontière entre ces deux régions n’est pas connue. 

On veut que quand on présentera un nouveau point \`a l'algorithme dont on ne connaît que la position dans le plan, l’algorithme de classification sera capable de prédire si ce nouveau point est un carré rouge ou un rond bleu.

\begin{figure}[h!]
	\centering
	\includegraphics[width=0.70\textwidth]{../ch4/data/svm1}
	\caption{L'objet \`a classer est un triangle}
	\label{svm_m}
\end{figure}
\FloatBarrier

Le problème de classification revient \`a trouver pour chaque nouvelle entrée, la catégorie \`a laquelle elle appartient. Autrement dit, il faut être capable de trouver la frontière entre les différentes catégories. Si on connaît la frontière, savoir de quel côté de la frontière appartient le point, et donc à quelle catégorie il appartient.

Le SVM est une solution à ce problème de classification. La première idée clé est la notion de marge maximale. La marge est la distance entre la frontière de séparation et les échantillons les plus proches. Ces derniers sont appelés vecteurs supports. Dans les SVM, la frontière de séparation est choisie comme celle qui maximise la marge \cite{zeste_key}.

\begin{figure}[h!]
	\centering
	\includegraphics[width=0.70\textwidth]{../ch4/data/svm2}
	\caption{Resultat de l'algorithme SVM}
\end{figure}
\FloatBarrier


\subsection{La méthode}

On observe pour les sources de tremblement de terre la puissance des signaux d'onde P et S peut varier pour des stations situées à peu près à la même distance épicentrale, mais dans différentes directions d'azimut. Les ondes sismiques des contiennent une large fréquence et leur énergie est répartie uniformément sur toute la bande de fréquence enregistrée. Les tremblements de terre produisent aussi formes d'onde plutôt appelés  P coda et S coda\footnote{La coda est expliquée par la diffusion multiple des ondes dans la croûte, ces phases de la coda contiennent toute l’information relative aux hétérogénéités ponctuelles du milieu de propagation}.
Contrairement aux tremblements de terre, les explosions sont des sources ponctuelles de compression à partir desquelles l'énergie des ondes P rayonne uniformément dans toutes les directions azimutales. Les ondes S sont vraisemblablement générées par des conversions de mode proches de la source. Ils ont une plus faible teneur en énergie ainsi que des fréquences dominantes inférieures aux ondes P correspondantes. Par rapport à un tremblement de terre de magnitude similaire, les explosions ont un contenu fréquentiel plus étroit ainsi qu'une durée plus courte pour les P coda et S coda \cite{svm_class_key}. 

Ainsi, le problème de classification repose sur les différences de distribution d'énergie du signal entre les sources sismiques naturelles et artificielles. 

Pour résoudre le problème en utilisant l'algorithme SVM, on peut procéder par les étapes suivantes:

\begin{enumerate}
	\item Choix des filtres
	\item Calcul par l'algorithme STA pour chaque canal filtré et les phases
	\item Application du nombre discriminant trouvé \`a l'algorithme SVM (Apprentissage)
	\item Classification par l'algorithme
	\item Interprétation des résultats
\end{enumerate}


