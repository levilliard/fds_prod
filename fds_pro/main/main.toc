\boolfalse {citerequest}\boolfalse {citetracker}\boolfalse {pagetracker}\boolfalse {backtracker}\relax 
\defcounter {refsection}{0}\relax 
\select@language {french}
\defcounter {refsection}{0}\relax 
\contentsline {section}{Remerciements}{1}{section*.2}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{Table des mati\`eres}{3}{section*.3}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{Table des figures}{5}{section*.4}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{Liste des tableaux}{8}{section*.5}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\chapternumberline {1}Pr\IeC {\'e}sentation du projet et mise en contexte}{9}{chapter.1}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {1.1}Introduction}{9}{section.1.1}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {1.2}Probl\IeC {\'e}matique soulev\IeC {\'e}e}{11}{section.1.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{Contexte g\IeC {\'e}ologique}{11}{section*.7}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{Vuln\IeC {\'e}rabilit\IeC {\'e}s}{12}{section*.9}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {1.3}Objectif g\IeC {\'e}n\IeC {\'e}ral}{14}{section.1.3}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {1.4}Objectifs sp\IeC {\'e}cifiques}{14}{section.1.4}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {1.5}Strat\IeC {\'e}gies}{15}{section.1.5}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\chapternumberline {2}Les syst\IeC {\`e}mes de surveillance}{16}{chapter.2}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.1}Introduction}{16}{section.2.1}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.2}Historique des r\IeC {\'e}seaux de surveillance sismique}{16}{section.2.2}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.3}Le r\IeC {\'e}seau sismique japonais(Hi-net, F-net, K-NET/KiK-net)}{17}{section.2.3}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{Le r\IeC {\'e}seau KiK-net}{17}{section*.11}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{Le reseau K-NET}{19}{section*.14}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{Collection et publication de donn\IeC {\'e}es}{19}{section*.16}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.4}Le programme GEOFON}{19}{section.2.4}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{Le r\IeC {\'e}seau permanent}{20}{section*.17}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{Processeur de communication sismologique(SeisComp)}{22}{section*.20}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{Extension du r\IeC {\'e}seau: Le r\IeC {\'e}seau virtuel GEVN}{23}{section*.22}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{Distribution de donn\IeC {\'e}es}{24}{section*.23}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.5}Le r\IeC {\'e}seau sismique mise au point en Ha\IeC {\"\i }ti}{24}{section.2.5}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{Le r\IeC {\'e}seau de sismologique d'Haiti}{24}{section*.24}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{A. Le r\IeC {\'e}seau de sismom\IeC {\`e}tres}{24}{section*.25}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{ B. Le r\IeC {\'e}seau d\IeC {\textquoteright }acc\IeC {\'e}l\IeC {\'e}rom\IeC {\`e}tres }{25}{section*.26}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{C. Le r\IeC {\'e}seau de GPS}{25}{section*.27}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{Le Centre National de Donn\IeC {\'e}es}{26}{section*.29}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{La gestion des flux de donn\IeC {\'e}es}{26}{section*.30}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.6}Synth\IeC {\`e}se et Perspectives }{27}{section.2.6}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\chapternumberline {3}\IeC {\'E}tude d'un syst\IeC {\`e}me de surveillance sismique}{29}{chapter.3}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {3.1}Introduction}{29}{section.3.1}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {3.2}Les stations sismiques}{30}{section.3.2}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {3.3}Les types de stations sismiques}{31}{section.3.3}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {3.4}Acquisition de donn\IeC {\'e}es}{32}{section.3.4}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{Syst\IeC {\`e}mes analogiques}{32}{section*.32}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{Le syst\IeC {\`e}me mixte (analogique/num\IeC {\'e}rique)}{33}{section*.33}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{Le syst\IeC {\`e}me num\IeC {\'e}rique}{33}{section*.34}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{Quelques capteur sismiques disponibles sur le march\IeC {\'e}}{33}{section*.35}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {3.5}Transmission de donn\IeC {\'e}es en sismologie}{35}{section.3.5}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {3.6}Stockage des flux de donn\IeC {\'e}es}{36}{section.3.6}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{Localisation des s\IeC {\'e}ismes}{37}{section*.38}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{Classification des s\IeC {\'e}ismes}{39}{section*.40}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {3.7}Les algorithmes de d\IeC {\'e}tection d'\IeC {\'e}v\IeC {\'e}nement sismique}{39}{section.3.7}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{d\IeC {\'e}clencheur de niveau ou seuil de d\IeC {\'e}clenchement d'amplitude: }{40}{section*.43}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{Aglorithmes STA/LTA(Short Time Average/Long Time Average: }{41}{section*.44}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {3.8}Syst\IeC {\`e}me de gestion de flux de donn\IeC {\'e}es}{41}{section.3.8}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\chapternumberline {4}Analyse et traitement des signaux sismiques}{43}{chapter.4}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {4.1}Introduction}{43}{section.4.1}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {4.2}Traitement des flux de donn\IeC {\'e}es sismiques}{44}{section.4.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{Premi\IeC {\`e}re phase: Pr\IeC {\'e}-traitement}{44}{section*.45}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{L\IeC {\textquoteright }\IeC {\'e}dition}{44}{section*.46}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{Filtre en fr\IeC {\'e}quence et en vitesse apparente}{44}{section*.47}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{Deuxi\IeC {\`e}me phase : corrections diverses et analyse de vitesse}{45}{section*.49}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{Troisi\IeC {\`e}me phase : les sommations}{45}{section*.50}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{Quatri\IeC {\`e}me phase: migration et inversion}{45}{section*.51}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{Migration}{45}{section*.52}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{Inversion}{45}{section*.53}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{Cinqui\IeC {\`e}me phase : estimation des zones r\IeC {\'e}servoirs}{46}{section*.54}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{S\IeC {\'e}paration d'ondes}{46}{section*.55}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{Filtrage}{46}{section*.56}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {4.3}Gestion d'\IeC {\'e}v\IeC {\'e}nements sismiques}{48}{section.4.3}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {4.4}Classification des signaux par l'algorithme SVM}{49}{section.4.4}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{Presentation de l'agorithme}{49}{section*.57}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{La m\IeC {\'e}thode}{51}{section*.60}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\chapternumberline {5}Analyse des param\IeC {\'e}tres de base du syst\IeC {\`e}me}{53}{chapter.5}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {5.1}Les param\IeC {\`e}tres de base du syst\IeC {\`e}me propos\IeC {\'e}}{53}{section.5.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{Les besoins du syst\IeC {\`e}me}{55}{section*.62}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{Recueillement de donn\IeC {\'e}es: }{55}{section*.63}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{Transmission de donn\IeC {\'e}es: }{55}{section*.64}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{Analyse de donn\IeC {\'e}es: }{55}{section*.65}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{Besoins non fonctionnels}{55}{section*.66}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {5.2}Recommandations}{56}{section.5.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{Recommandations g\IeC {\'e}n\IeC {\'e}rales}{56}{section*.67}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{S\IeC {\'e}lection du vendeur }{56}{section*.68}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{Choix des capteurs et Recommandation }{57}{section*.69}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {5.3}R\IeC {\'e}alit\IeC {\'e} Financi\IeC {\`e}re}{58}{section.5.3}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{Exemple de cout d'installation d'un syst\IeC {\`e}me d'acquisition de sismologiques en Haiti}{59}{section*.71}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{A: Acquisition d\IeC {\textquoteright }\IeC {\'e}quipements}{60}{section*.72}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{B: Am\IeC {\'e}nagement de locaux; 13.000 USD }{61}{section*.73}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\chapternumberline {6}Conception du syst\IeC {\`e}me}{62}{chapter.6}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {6.1}Architecture r\IeC {\'e}seau}{62}{section.6.1}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {6.2}Mod\IeC {\`e}le conceptuel de la communication}{63}{section.6.2}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {6.3}Diagramme fonctionnel}{65}{section.6.3}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {6.4}M\IeC {\'e}thodologie}{65}{section.6.4}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{Cycle de d\IeC {\'e}veloppement logiciel: Mod\IeC {\`e}le en V}{66}{section*.77}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{M\IeC {\'e}thode de d\IeC {\'e}veloppement: M\IeC {\'e}thode RUP}{67}{section*.79}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{Langage de d\IeC {\'e}veloppement: UML}{67}{section*.80}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{Les points forts d'UML:}{67}{section*.81}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{Les points faibles d'UML:}{67}{section*.82}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {6.5}Diagramme des cas d\IeC {\textquoteright }utilisation}{68}{section.6.5}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{Identification des acteurs du syst\IeC {\`e}me}{68}{section*.83}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{Identification des cas d'utilisation}{68}{section*.84}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{Diagramme des cas d'utilisation}{69}{section*.85}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {6.6}Diagramme d'activit\IeC {\'e}}{72}{section.6.6}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{Composition d'un diagramme d'activit\IeC {\'e}}{72}{section*.90}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{Les activit\IeC {\'e}s de notre syst\IeC {\`e}me}{72}{section*.94}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{Le diagramme des activit\IeC {\'e}s}{74}{section*.95}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{Diagramme d'Authentification:}{74}{section*.96}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{Diagramme de traitement de donn\IeC {\'e}es:}{74}{section*.98}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{Diagramme de gestion des stations:}{75}{section*.100}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {6.7}Diagramme de sequence}{76}{section.6.7}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{Composition d\IeC {\textquoteright }un diagramme de s\IeC {\'e}quences}{77}{section*.102}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{Diagramme de s\IeC {\'e}quence de notre syst\IeC {\`e}me}{77}{section*.103}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{Diagramme de s\IeC {\'e}quence de cr\IeC {\'e}ation d'utilisateur}{77}{section*.104}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{Diagramme de sequence d'authentification}{78}{section*.106}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{Diagramme de sequence de la gestion des stations}{79}{section*.108}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{Diagramme de sequence de gestion des flux de donn\IeC {\'e}es}{79}{section*.110}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {6.8}Mod\IeC {\'e}lisation conceptuelle des donn\IeC {\'e}es}{80}{section.6.8}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{Diagramme de classe}{81}{section*.112}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{Composition d'un diagramme de classe}{81}{section*.113}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{Diagramme de classe du syst\IeC {\`e}me}{81}{section*.114}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{ Sch\IeC {\'e}ma Relationnel }{82}{section*.116}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{Description des tables }{84}{section*.118}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\chapternumberline {7}Impl\IeC {\'e}mentation du syst\IeC {\`e}me }{88}{chapter.7}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {7.1}Choix du capteur}{88}{section.7.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{Nanometrics Trillium 120PH}{88}{section*.126}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{Performances du Trillium 120PH}{89}{section*.127}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{Variation du bruit en fonction de la profondeur}{89}{section*.129}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{CMG-3T (Guralp)}{92}{section*.134}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {7.2}Installation du CMG-3T}{95}{section.7.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{Notes d'instruction }{99}{section*.141}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{Alimentation \IeC {\'E}lectrique }{99}{section*.142}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{ Signal de sortie }{100}{section*.144}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{Connexion}{101}{section*.145}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{Utilisation d'un num\IeC {\'e}riseur}{101}{section*.146}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{Installation}{102}{section*.148}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{Calibration}{103}{section*.149}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {7.3}Installation et configuration du num\IeC {\'e}riseur}{105}{section.7.3}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{Aper\IeC {\c c}u}{105}{section*.150}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{Alimentation \IeC {\'E}lectrique}{105}{section*.151}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{Connexion \IeC {\`a} un seul ordinateur}{106}{section*.152}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{Connexion sur un lien s\IeC {\'e}rie: }{106}{section*.153}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{Pour communiquer sur un lien s\IeC {\'e}rie:}{106}{section*.154}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{Connexion via Ethernet}{106}{section*.155}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{Utilisation d'un modem interne: }{107}{section*.156}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{Connexion sur USB:}{107}{section*.157}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {7.4}Syst\IeC {\`e}me d'alimentation \IeC {\'e}lectrique}{107}{section.7.4}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {7.5}Configuration des stations}{108}{section.7.5}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {7.6}Implementation du systeme de gestion de flux}{109}{section.7.6}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {7.7}Environnement de r\IeC {\'e}alisation }{110}{section.7.7}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{Mat\IeC {\'e}riel de base}{110}{section*.160}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{Choix des langage de programmation et biblioth\IeC {\`e}ques}{110}{section*.162}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{Framework}{110}{section*.163}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{Choix du SGBD}{111}{section*.164}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{Environnement de developpement}{113}{section*.165}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {7.8}\IeC {\'E}tude d'enchainement du programme}{113}{section.7.8}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {7.9}Simulation}{115}{section.7.9}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{Description du logiciel de traitement de flux de donn\IeC {\'e}es}{115}{section*.167}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{Pr\IeC {\'e}sentation Service Web}{115}{section*.168}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{Pr\IeC {\'e}sentation des interfaces graphiques}{116}{section*.170}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{Analyse et Traitement de flux de donn\IeC {\'e}es}{116}{section*.171}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{Quelques captures d'\IeC {\'e}crans }{117}{section*.173}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{Glossaire}{120}{section*.176}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{Bibliographie}{122}{section*.179}
