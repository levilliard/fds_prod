#include "unetwork.h"

#include <QJsonArray>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonValue>

#include <QNetworkAccessManager>
#include <QNetworkReply>
#include <QNetworkRequest>
#include <QUrl>
#include <QScriptEngine>
#include <QScriptValueIterator>
#include <QMessageBox>

#include <iostream>
#include <fstream>

#include <QPushButton>

#include <QDir>

UNetwork::UNetwork(QWidget * parent)
    : QWidget(parent)
    , mNetMan(new QNetworkAccessManager(this))
    , mNetReply(nullptr)
    , mDataBuffer(new QByteArray){

    /*
     *     "networkId": "UI";
           "startDate": "1993-01-01";
           "endDate": "2002-01-01";
    */

    //| url: "http://localhost:8080/urgeosismo/urgeosismo/network/station/all/"
    //| data: "{\"networkId\": \"IU\", \"startDate\": \"1993-01-01\", \"endDate\": \"2002-01-01d\"}"
}

void UNetwork::initRequest(QString &uri, QString &data){

    QUrl url(uri);
    QNetworkRequest req;
    QByteArray jsonDocument(data.toStdString().c_str());

    req.setHeader(QNetworkRequest::ContentTypeHeader, "application/json");

    req.setUrl(url);
    mNetReply = mNetMan->post(req, jsonDocument);
    connect(mNetMan, SIGNAL(finished(QNetworkReply*)), this, SLOT(onResult(QNetworkReply*)));
}

UNetwork::~UNetwork(){
   if(mNetReply){
       delete mNetReply;
   }

   if(mNetMan){
       delete mNetMan;
   }
}

void UNetwork::onResult(QNetworkReply* reply)
{
    std::cout<<"Retrieve data....\n";

    if(directory.length() <= 0 || file_name.length() <= 0){
        std::cout<<"file or directory not specifie\n";
        return;
    }

    if (mNetReply->error() != QNetworkReply::NoError){
        std::cout<<mNetReply->errorString().toStdString()<<"\n";
        return;
    }

    QString data = (QString)reply->readAll();

    QDir dir(directory);

    if(!dir.exists()){
        dir.mkdir(directory);
    }

    std::ofstream m_file;
    std::string data_file = directory.toStdString() + "/" + file_name.toStdString();
    m_file.open(data_file, std::fstream::in | std::fstream::out | std::fstream::app);

    if (!m_file )
    {
      m_file.open(data_file,  std::fstream::in | std::fstream::out | std::fstream::trunc);
      m_file <<data.toStdString();
      m_file.close();

     }
    else
    {
       // use existing file
       //m_file << data.toStdString();
       m_file.close();
       //std::cout<<"\n";
    }
}

void UNetwork::initDirectory(QString dir, QString file){
    std::cout<<"file and directory:"<<dir.toStdString()<<", "<<file.toStdString()<<"\n";
    directory = dir;
    file_name = file;
}
