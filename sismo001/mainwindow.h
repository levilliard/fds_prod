#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "./windows/stationviz.h"
#include "./windows/svm.h"
#include "./windows/eventloc.h"
#include "./windows/data_viz/datawidget.h"

#include <QMainWindow>
#include <QWidget>
#include <QStackedWidget>

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = 0);
    ~MainWindow();

public slots:
    void goSvm();
    void goStation();
    void goDataViz();
    void goEvents();
    void showMsg();


private:
    void createMenu();

    QWidget* central;
    QWidget* main_menu;

    Svm* svm;
    EventLoc* evt_loc;
    StationViz* stations;
    DataWidget* waves;
    QStackedWidget* stack;
};

#endif // MAINWINDOW_H
