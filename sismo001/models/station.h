
/*
 * 	\hline \hline
    station{\_}id & Identifiant unique d'une instance de la classe Station\\
    \hline
    station{\_}code & le code la station\\
    \hline
    station{\_}loc & localisation géographique de la station\\
    \hline
    station{\_}elevation & élévation\\
    \hline
    station{\_}desc & description de la station\\
    \hline
    network{\_}remark & remarques\\
 */


#ifndef STATION_H
#define STATION_H

#include <string>

class Station
{
    public:
        Station(const std::string& id, const std::string& code, double lat, double lon, const std::string& elv, const std::string& desc, const std::string& rm, const std::string& lm) {
            stationId = id;
            stationCode = code;
            stationLat = lat;
            stationLon = lon;
            stationElevation = elv;
            stationDesc = desc;
            stationRemark = rm;
            stationLastModified = lm;
        }

        void setStationId(std::string& id){
            stationId = id;
        }

        std::string getStaionId(){
            return stationId;
        }

        void setStationCode(std::string& code){
            stationCode = code;
        }

        std::string getStationCode(){
            return stationCode;
        }

        void setStationLat(double lat){
            stationLat = lat;
        }

        double getStationLat(){
            return stationLat;
        }


        void setStationLon(double lon){
            stationLon = lon;
        }

        double getStationLon(){
            return stationLon;
        }

        void setStationElevation(std::string& elevation){
            stationElevation = elevation;
        }

        std::string getStationElevation(){
            return stationElevation;
        }



        void setStationDesc(std::string& desc){
            stationDesc = desc;
        }

        std::string getStationDesc(){
            return stationDesc;
        }



        void setStationRemark(std::string& remark){
            stationRemark = remark;
        }

        std::string getStationRemark(){
            return stationRemark;
        }



        void setStationLastModified(std::string& lastModified){
            stationLastModified = lastModified;
        }

        std::string getStationLastModified(){
            return stationLastModified;
        }


    private:
        std::string stationId;
        std::string stationCode;
        double stationLat;
        double stationLon;
        std::string stationElevation;
        std::string stationDesc;
        std::string stationRemark;
        std::string stationLastModified;
};
#endif  //STATION_H
