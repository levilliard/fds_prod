

#ifndef NETWORK_H
#define NETWORK_H

#include <string>

using namespace std;

class Network
{
   public:
        Network(){}

        Network(const string& id, const string& code, const string& inst, const string& reg, const string& modif, const string& rem){
            networkId = id;
            networkCode = code;
            networkInstitution = inst;
            networkRegion = reg;
            networkLastModified = modif;
            networkRemarks = rem;
        }

        void setNetworkId(std:: string& id){
            networkId = id;
        }

        std::string getNeworkId(){
            return networkId;
        }

        void setNetworkCode(std::string& code){
            networkCode = code;
        }

        std::string getNetworkCode(){
            return networkCode;
        }

        void setNwetworkInstitution(std::string& institution){
            networkInstitution = institution;
        }

        std::string getNetworkInstitution(){
            return networkInstitution;
        }

        void setNetworkRegion(std::string& region){
            networkRegion = region;
        }

        std::string getNetworkRegion(){
            return networkRegion;
        }

        void setNetworkLastModified(std::string& lModified){
            networkLastModified = lModified;
        }

        std::string getNetworkLastModified(){
            return networkLastModified;
        }

        void setNetworkRemarks(std::string& remarks){
            networkRemarks = remarks;
        }

        std::string getNetworkRemarks(){
            return networkRemarks;
        }

   private:
        std::string networkId;
        std::string networkCode;
        std::string networkInstitution;
        std::string networkRegion;
        std::string networkLastModified;
        std::string networkRemarks;
};

#endif  //NETWORK_H
