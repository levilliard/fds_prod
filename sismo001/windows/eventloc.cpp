#include "eventloc.h"
#include <QWidget>
#include <QSplitter>
#include <QHBoxLayout>
#include <QDialogButtonBox>
#include <QLabel>
#include <QLineEdit>
#include <QPushButton>

#include "Basemap.h"
#include "Map.h"
#include "MapGraphicsView.h"
#include <SimpleMarkerSymbol.h>

using namespace Esri::ArcGISRuntime;

EventLoc::EventLoc(QWidget *parent) : QWidget(parent), m_map(nullptr), m_mapView(nullptr)
{
    this->setStyleSheet("border: 0");
    //central widget

    // Create the Widget view
    m_mapView = new MapGraphicsView(this);
    m_mapView->setWrapAroundMode(WrapAroundMode::Disabled);

    // Create a map using the nationalGeographic BaseMap
    m_map = new Map(Basemap::nationalGeographic(this), this);

    // Set map to map view
    m_mapView->setMap(m_map);
    m_mapView->setMinimumSize(600, 500);

    SimpleMarkerSymbol* buoySymbol = new SimpleMarkerSymbol(SimpleMarkerSymbolStyle::Circle, QColor(Qt::red), 10);
    GraphicsOverlay* graphicsOverlay = new GraphicsOverlay(this);
    QList<Point> buoyLocations;


    populateBuoyLocations(buoyLocations);
    for (const auto& location : buoyLocations)
    {
        graphicsOverlay->graphics()->append(new Graphic(location, buoySymbol, this));
    }

    m_mapView->graphicsOverlays()->append(graphicsOverlay);

    //menu widget
    menu = new QWidget(this);
    menu->setMaximumWidth(200);
    menu->setStyleSheet("color: white;""background-color: #333;");
    createMenu();

    log = new QPlainTextEdit(this);
    log->setStyleSheet("color: #fff;" "background-color: #000;");
    //log->setMinimumHeight(200);
    log->setReadOnly(true);
    //QString now = " " + QDateTime::currentDateTime().toString();
    QString str = "Events location";
    log->appendHtml("<font color=green> " + str + "</font>");

    QSplitter* sp = new QSplitter(Qt::Vertical);
    sp->addWidget(m_mapView);
    sp->addWidget(log);

    QHBoxLayout* hl = new QHBoxLayout(this);
    hl->addWidget(sp);
    hl->addWidget(menu);
    this->setLayout(hl);
}

void EventLoc::createMenu(){
    this->m_form = new QFormLayout(menu);
    m_form->addRow(new QLabel("Request parameters"));

    QString strs[] = {"Max Lat", "Min Lat", "Min Mag"};

    QList<QLineEdit *> chp;
    for(int i = 0; i < 3; ++i) {
        QLineEdit *lEdit = new QLineEdit(this);
        lEdit->setStyleSheet("color: #fff;" "background-color: #555;");
        m_form->addRow(strs[i], lEdit);
        chp<<lEdit;
    }

    QPushButton* btn = new QPushButton("Send", this);
    btn->setStyleSheet("color: #fff;" "background-color: #555;");

    btn->setMaximumWidth(100);
    m_form->addRow(btn);
    QObject::connect(btn, SIGNAL(accepted()), this, SLOT(accept()));
    QObject::connect(btn, SIGNAL(rejected()), this, SLOT(reject()));
}

void EventLoc::populateBuoyLocations(QList<Point>& locations)
{
  SpatialReference sr(4326);
  locations.clear();
  locations.append(Point(-2.712642647560347, 56.062812566811544, sr));
  locations.append(Point(-2.6908416959572303, 56.06444173689877, sr));
  locations.append(Point(-2.6697273884990937, 56.064250073402874, sr));
  locations.append(Point(-2.6395150461199726, 56.06127916736989, sr));
}
