#ifndef EVENTLOC_H
#define EVENTLOC_H

#include <QWidget>
#include <QPlainTextEdit>
#include <QFormLayout>
#include <Point.h>

namespace Esri
{
    namespace ArcGISRuntime
    {
        class Map;
        class MapGraphicsView;
    }
}

class EventLoc : public QWidget
{
    Q_OBJECT
public:
    explicit EventLoc(QWidget *parent = 0);
    void createMenu();
    void populateBuoyLocations(QList<Esri::ArcGISRuntime::Point>& locations);

signals:

public slots:

private:
    QWidget* central;
    QPlainTextEdit* log;
    QWidget* menu;
    QFormLayout* m_form;
    Esri::ArcGISRuntime::Map* m_map;
    Esri::ArcGISRuntime::MapGraphicsView* m_mapView;

};

#endif // EVENTLOC_H
