
/*
 * URGEO Seismic Data Analysis
 * October 2016
 * levilliard@gmail.com
 * -----------------------------------------
 *
 */

#ifndef EVENTVIZ_H
#define EVENTVIZ_H

#include <QObject>
#include <QWidget>

#include "../map/MMap.h"
#include "../models/station.h";
#include "../models/network.h"
#include "../unetwork.h"

#include <QPushButton>
#include <vector>
#include <QPlainTextEdit>
#include <QComboBox>

class QByteArray;
class QNetworkAccessManager;
class QNetworkReply;
class QPushButton;

class StationViz: public QWidget
{
    Q_OBJECT

    public:
        StationViz(QWidget* parent=nullptr);
        ~StationViz();

    public slots:
        void resetCamera();
        void selectNetwork(int code);

    private:
        MMap* map;
        QWidget* menu;
        std::vector<Station> slist;
        std::vector<Network> nlist;
        QPushButton*  reset;
        QPlainTextEdit* log;
        QComboBox* network_codes;
        UNetwork* unet;

        void initPosition();
        void initNetworkList();
};

#endif // EVENTVIZ_H
