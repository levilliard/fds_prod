#-------------------------------------------------
#
# Project created by QtCreator 2017-09-14T12:12:54
#
#-------------------------------------------------

TARGET = sismo001
TEMPLATE = app

QT += positioning core gui opengl network widgets sensors script

CONFIG += c++11

ARCGIS_RUNTIME_VERSION = 100.1
include($$PWD/arcgisruntime.pri)

win32:CONFIG += \
    embed_manifest_exe


SOURCES += main.cpp\
    mainwindow.cpp \
    windows/data_viz/datawidget.cpp \
    windows/data_viz/plotsettings.cpp \
    windows/data_viz/qplotter.cpp \
    windows/data_viz/stationconfig.cpp \
    windows/eventloc.cpp \
    windows/stationviz.cpp \
    windows/svm.cpp \
    models/network.cpp \
    models/station.cpp \
    map/MMap.cpp \
    unetwork.cpp

HEADERS  += mainwindow.h \
    windows/data_viz/datawidget.h \
    windows/data_viz/plotsettings.h \
    windows/data_viz/plotter.h \
    windows/data_viz/stationconfig.h \
    windows/eventloc.h \
    windows/stationviz.h \
    windows/svm.h \
    models/network.h \
    models/station.h \
    global/global.h \
    map/MMap.h \
    unetwork.h
