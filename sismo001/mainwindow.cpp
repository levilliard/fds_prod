
#include "mainwindow.h"
#include <QSplitter>
#include <QHBoxLayout>
#include <QPushButton>
#include <QGridLayout>
#include <QMessageBox>

MainWindow::MainWindow(QWidget *parent): QMainWindow(parent){

    this->setWindowTitle("URGEO-SISM0v1.02");
    //this->setStyleSheet("color: white;""background-color: #121212;");
    this->setMinimumSize(800, 600);

    // central widget
    this->central = new QWidget(this);

    //content
    stack = new QStackedWidget(central);
    svm = new Svm(central);
    stations = new StationViz(central);
    evt_loc = new EventLoc(central);
    waves = new DataWidget(central);

    stack->addWidget(stations);
    stack->addWidget(svm);
    stack->addWidget(evt_loc);
    stack->addWidget(waves);

    //menu
    this->createMenu();

    //spliter
    QSplitter* sph = new QSplitter(Qt::Horizontal);
    sph->addWidget(main_menu);
    sph->addWidget(stack);

    QHBoxLayout* hl = new QHBoxLayout();
    hl->addWidget(sph);

    this->central->setLayout(hl);
    this->setCentralWidget(central);
}

MainWindow::~MainWindow(){

}

void MainWindow::showMsg(){
    QMessageBox::about(this, "FDS-SISMOv0.02", "This project is for seismic data analysis");
}

void MainWindow::createMenu(){
    this->main_menu = new QWidget(this);
    this->main_menu->setStyleSheet("color: white;""background-color: #1d1c1c;");
    this->main_menu->setMaximumWidth(200);

    //menu->setStyleSheet("border: solid 4px #222;");

    QPushButton* data = new QPushButton("Wave", main_menu);
    data->setStyleSheet("color: white;""background-color: #555;" "border:0");
    data->setFixedHeight(50);
    //data->setIcon(style()->standardIcon(QStyle::SP_TitleBarMinButton));
    QObject::connect(data, SIGNAL(clicked()), this, SLOT(goDataViz()));

    QPushButton* svm = new QPushButton("SVM", main_menu);
    svm->setStyleSheet("color: white;""background-color: #555;" "border:0");
    svm->setFixedHeight(50);
    QObject::connect(svm, SIGNAL(clicked()), this, SLOT(goSvm()));


    QPushButton* event = new QPushButton("Events", main_menu);
    event->setStyleSheet("color: white;""background-color: #555;" "border:0");
    event->setFixedHeight(50);
    QObject::connect(event, SIGNAL(clicked()), this, SLOT(goEvents()));


    QPushButton* station = new QPushButton("Stations", main_menu);
    station->setStyleSheet("color: white;""background-color: #555;" "border:0");
    station->setFixedHeight(50);
    QObject::connect(station, SIGNAL(clicked()), this, SLOT(goStation()));

    QPushButton* log = new QPushButton("Log", main_menu);

    //other button
    QPushButton* help = new QPushButton("Help", main_menu);
    QObject::connect(help, SIGNAL(clicked), this, SLOT(showMsg()));
    QPushButton* oother = new QPushButton("Login", main_menu);

    QGridLayout *grid = new QGridLayout();
    grid->addWidget(data, 0, 0);
    grid->addWidget(svm, 0, 1);
    grid->addWidget(event, 1, 0);
    grid->addWidget(station, 1, 1);

    //main manu
    QWidget* wd = new QWidget(main_menu);
    wd->setFixedHeight(200);
    wd->setStyleSheet("color: white;""background-color: #222;");
    wd->setLayout(grid);
    //wd->move(10, 10);

    //other menu
    QWidget* oth = new QWidget(main_menu);
    oth->setStyleSheet("color: white;""background-color: #222;");
    oth->setFixedHeight(200);
    QHBoxLayout* ohl = new QHBoxLayout(main_menu);
    ohl->addWidget(help);
    ohl->addWidget(oother);
    oth->setLayout(ohl);

    QVBoxLayout* ml = new QVBoxLayout(main_menu);
    ml->addWidget(wd);
    ml->addWidget(log);
    ml->addWidget(oth);
    wd->setLayout(ml);
}

void MainWindow::goDataViz(){
    this->stack->setCurrentWidget(stack->widget(3));
}

void MainWindow::goSvm(){
    this->stack->setCurrentWidget(stack->widget(1));
}

void MainWindow::goStation(){
    this->stack->setCurrentWidget(stack->widget(0));
}

void MainWindow::goEvents(){
    this->stack->setCurrentWidget(stack->widget(2));
}
