
/*
 * URGEO Seismic Data Analysis
 * October 2016
 * levilliard@gmail.com
 * -----------------------------------------
 *
 * usage:
 *  1. Create an object
 *  2. Call the method initDirectory(...)
 *  3. Call the method  initRequest(...)
 *
 */



#ifndef UNETWORK_H
#define UNETWORK_H

#include <QWidget>

class QByteArray;
class QNetworkAccessManager;
class QNetworkReply;
class QPushButton;

class UNetwork : public QWidget
{
    Q_OBJECT

public:
    UNetwork(QWidget *parent = 0);
    ~UNetwork();

    /*
     * initialization of the http request, a post http request require a url and data parameters
     *  - url: url
     *  - parameter: data
     * The request will handle to our web serve api create on top of the IRIS_WS
     *
     */
    void initRequest(QString &uri, QString &data);
    void initDirectory(QString dir, QString file);

public slots:
    /*
     * When the request is complete, we have to store data localy
     *  - we create a directory (if not exist) with the network name
     *  - we create a file (if not exist) with all the network's stations
     *  - we create a data file (if not exist) for every station
     */
    void onResult(QNetworkReply* reply);

private:
    QNetworkAccessManager * mNetMan;
    QNetworkReply * mNetReply;
    QByteArray * mDataBuffer;

    QString directory;
    QString file_name;
};

#endif // UNETWORK_H
