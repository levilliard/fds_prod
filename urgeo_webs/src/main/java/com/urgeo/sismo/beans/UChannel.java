/**
 *
 * FDS PRO FIN 2016
 * SEISMIC WEBSERVICE
 * @author levilliard
 *
 */

package com.urgeo.sismo.beans;

public class UChannel {
    
    private String code;
    private String dateOn;
    private String dateOff;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDateOn() {
        return dateOn;
    }

    public void setDateOn(String dateOn) {
        this.dateOn = dateOn;
    }

    public String getDateOff() {
        return dateOff;
    }

    public void setDateOff(String dateOff) {
        this.dateOff = dateOff;
    }

    @Override
    public String toString() {
        return "Channel{" + "code=" + code + ", dateOn=" + dateOn + ", dateOff=" + dateOff + '}';
    }
    
    
}
