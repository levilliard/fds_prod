/**
 *
 * FDS PRO FIN 2016
 * SEISMIC WEBSERVICE
 * @author levilliard
 *
 */

package com.urgeo.sismo.beans;

public class Wave {
    
    private String networkId;
    private String stationId;
    private String waveLoc;
    private String channelId;
    private String startDate;
    private String endDate;

    public String getNetworkId() {
        return networkId;
    }

    public void setNetworkId(String networkId) {
        this.networkId = networkId;
    }

    public String getStationId() {
        return stationId;
    }

    public void setStationId(String stationId) {
        this.stationId = stationId;
    }

    public String getWaveLoc() {
        return waveLoc;
    }

    public void setWaveLoc(String waveLoc) {
        this.waveLoc = waveLoc;
    }

    public String getChannelId() {
        return channelId;
    }

    public void setChannelId(String channelId) {
        this.channelId = channelId;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }
    
}
