/**
 *
 * FDS PRO FIN 2016
 * SEISMIC WEBSERVICE
 * @author levilliard
 *
 */

package com.urgeo.sismo.beans;

import java.util.List;

public class UEvent {
    private String eventId;
    private String eventCreationDate;
    private String eventType;
    private String preferedMagnitude;
    private String eventOrigin;
    private String eventDesc;
    private String eventRegionName;

    public String getEventRegionName() {
        return eventRegionName;
    }

    public void setEventRegionName(String eventRegionName) {
        this.eventRegionName = eventRegionName;
    }
   
    private List<UMagnitude> magList;

    public String getEventId() {
        return eventId;
    }

    public void setEventId(String eventId) {
        this.eventId = eventId;
    }

    public String getEventCreationDate() {
        return eventCreationDate;
    }

    public void setEventCreationDate(String eventCreationDate) {
        this.eventCreationDate = eventCreationDate;
    }

    public String getEventType() {
        return eventType;
    }

    public void setEventType(String eventType) {
        this.eventType = eventType;
    }

    public String getPreferedMagnitude() {
        return preferedMagnitude;
    }

    public void setPreferedMagnitude(String preferedMagnitude) {
        this.preferedMagnitude = preferedMagnitude;
    }

    public String getEventOrigin() {
        return eventOrigin;
    }

    public void setEventOrigin(String eventOrigin) {
        this.eventOrigin = eventOrigin;
    }

    public String getEventDesc() {
        return eventDesc;
    }

    public void setEventDesc(String eventDesc) {
        this.eventDesc = eventDesc;
    }

    public List<UMagnitude> getMagList() {
        return magList;
    }

    public void setMagList(List<UMagnitude> magList) {
        this.magList = magList;
    }

    @Override
    public String toString() {
        return "Event{" + "eventId=" + eventId + ", eventCreationDate=" + eventCreationDate + ", eventType=" + eventType + ", preferedMagnitude=" + preferedMagnitude + ", eventOrigin=" + eventOrigin + ", eventDesc=" + eventDesc + '}';
    }
}
