/**
 *
 * FDS PRO FIN 2016
 * SEISMIC WEBSERVICE
 * @author levilliard
 *
 */

package com.urgeo.sismo.beans;

public class UNetwork {
    String networkId;
    String networkCode;
    String networkInstitution;
    String networkRegion;
    String networkLastModified;
    String networkRemarks;

    public String getNetworkId() {
        return networkId;
    }

    public void setNetworkId(String networkId) {
        this.networkId = networkId;
    }

    public String getNetworkCode() {
        return networkCode;
    }

    public void setNetworkCode(String networkCode) {
        this.networkCode = networkCode;
    }

    public String getNetworkInstitution() {
        return networkInstitution;
    }

    public void setNetworkInstitution(String networkInstitution) {
        this.networkInstitution = networkInstitution;
    }

    public String getNetworkRegion() {
        return networkRegion;
    }

    public void setNetworkRegion(String networkRegion) {
        this.networkRegion = networkRegion;
    }

    public String getNetworkLastModified() {
        return networkLastModified;
    }

    public void setNetworkLastModified(String networkLastModified) {
        this.networkLastModified = networkLastModified;
    }

    public String getNetworkRemarks() {
        return networkRemarks;
    }

    public void setNetworkRemarks(String networkRemarks) {
        this.networkRemarks = networkRemarks;
    }

    @Override
    public String toString() {
        return "Network{" + "networkId=" + networkId + ", networkCode=" + networkCode + ", networkInstitution=" + networkInstitution + ", networkRegion=" + networkRegion + ", networkLastModified=" + networkLastModified + ", networkRemarks=" + networkRemarks + '}';
    }
}

