
/**
 *
 * FDS PRO FIN 2016
 * SEISMIC WEBSERVICE
 * @author levilliard
 *
 */

package com.urgeo.sismo.beans;

import java.util.List;

public class UStation {
    private String stationId;
    private String stationCode;
    private double stationLat;
    private double stationLon;
    private String stationElevation;
    private String stationDesc;
    private String  stationRemark;
    private String stationLastModified;
    private String stationStartDate;
    private String stationEndDate;
    private List<UChannel> channelList;
    private String createdBy;
    private String dateCreated;
    private String modifiedBy;
    private String dateModified; 
    
    public String getStationId() {
        return stationId;
    }

    public void setStationId(String stationId) {
        this.stationId = stationId;
    }

    public String getStationCode() {
        return stationCode;
    }

    public void setStationCode(String stationCode) {
        this.stationCode = stationCode;
    }

    public double getStationLat() {
        return stationLat;
    }

    public void setStationLat(double stationLat) {
        this.stationLat = stationLat;
    }

    public double getStationLon() {
        return stationLon;
    }

    public void setStationLon(double stationLon) {
        this.stationLon = stationLon;
    }

    public String getStationElevation() {
        return stationElevation;
    }

    public void setStationElevation(String stationElevation) {
        this.stationElevation = stationElevation;
    }

    public String getStationDesc() {
        return stationDesc;
    }

    public void setStationDesc(String stationDesc) {
        this.stationDesc = stationDesc;
    }

    public String getStationRemark() {
        return stationRemark;
    }

    public void setStationRemark(String stationRemark) {
        this.stationRemark = stationRemark;
    }

    public String getStationLastModified() {
        return stationLastModified;
    }

    public void setStationLastModified(String stationLastModified) {
        this.stationLastModified = stationLastModified;
    }

    public String getStationStartDate() {
        return stationStartDate;
    }

    public void setStationStartDate(String stationStartDate) {
        this.stationStartDate = stationStartDate;
    }

    public String getStationEndDate() {
        return stationEndDate;
    }

    public void setStationEndDate(String stationEndDate) {
        this.stationEndDate = stationEndDate;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(String dateCreated) {
        this.dateCreated = dateCreated;
    }

    public String getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    public String getDateModified() {
        return dateModified;
    }

    public void setDateModified(String dateModified) {
        this.dateModified = dateModified;
    }

    public List<UChannel> getChannelList() {
        return channelList;
    }

    public void setChannelList(List<UChannel> channelList) {
        this.channelList = channelList;
    }

    @Override
    public String toString() {
        return "Station{" + "stationId=" + stationId + ", stationCode=" + stationCode + ", stationLat=" + stationLat + ", stationLon=" + stationLon + ", stationElevation=" + stationElevation + ", stationDesc=" + stationDesc + ", stationRemark=" + stationRemark + ", stationLastModified=" + stationLastModified + '}';
    }
    
    
}
