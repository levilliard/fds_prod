/**
 *
 * FDS PRO FIN 2016
 * SEISMIC WEBSERVICE
 * @author levilliard
 *
 */

package com.urgeo.sismo.beans;

public class UEventCriteria {
    private double maxLatitude;
    private double minLongitude;
    private double minMagnitude;

    public double getMaxLatitude() {
        return maxLatitude;
    }

    public void setMaxLatitude(double maxLatitude) {
        this.maxLatitude = maxLatitude;
    }

    public double getMinLongitude() {
        return minLongitude;
    }

    public void setMinLongitude(double minLongitude) {
        this.minLongitude = minLongitude;
    }

    public double getMinMagnitude() {
        return minMagnitude;
    }

    public void setMinMagnitude(double minMagnitude) {
        this.minMagnitude = minMagnitude;
    }
}
