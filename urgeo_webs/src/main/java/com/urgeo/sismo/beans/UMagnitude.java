
/**
 *
 * FDS PRO FIN 2016
 * SEISMIC WEBSERVICE
 * @author levilliard
 *
 */

package com.urgeo.sismo.beans;

public class UMagnitude {
    
    private String magType;
    private double magValue;

    public String getMagType() {
        return magType;
    }

    public void setMagType(String magType) {
        this.magType = magType;
    }

    public double getMagValue() {
        return magValue;
    }

    public void setMagValue(double magValue) {
        this.magValue = magValue;
    }

}
