/**
 *
 * FDS PRO FIN 2016
 * SEISMIC WEBSERVICE
 * @author levilliard
 *
 */

package com.urgeo.sismo.controllers;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;
import java.util.ArrayList;

import javax.ws.rs.Produces;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.core.MediaType;

import edu.iris.dmc.criteria.CriteriaException;  
import edu.iris.dmc.criteria.OutputLevel;
import edu.iris.dmc.criteria.StationCriteria;
import edu.iris.dmc.fdsn.station.model.Channel;
import edu.iris.dmc.fdsn.station.model.Network;
import edu.iris.dmc.fdsn.station.model.Station;
import edu.iris.dmc.service.NoDataFoundException;
import edu.iris.dmc.service.ServiceUtil;
import edu.iris.dmc.service.StationService;

import com.urgeo.sismo.beans.UChannel;
import com.urgeo.sismo.beans.UEvent;
import com.urgeo.sismo.beans.UMagnitude;
import com.urgeo.sismo.beans.UStation;
import com.urgeo.sismo.beans.NetworkConfig;
import com.urgeo.sismo.beans.Wave;
import com.urgeo.sismo.beans.UEventCriteria;

import edu.iris.dmc.criteria.EventCriteria;
import edu.iris.dmc.criteria.WaveformCriteria;
import edu.iris.dmc.event.model.Event;
import edu.iris.dmc.event.model.Magnitude;
import edu.iris.dmc.service.EventService;
import edu.iris.dmc.service.WaveformService;
import edu.iris.dmc.timeseries.model.Segment;
import edu.iris.dmc.timeseries.model.Timeseries;
import javax.ws.rs.POST;

@Path("network")
public class NetworkCtrl  {
    
    ServiceUtil serviceUtil;

    public NetworkCtrl(){
        this.serviceUtil = ServiceUtil.getInstance();
        serviceUtil.setAppName("FDSISMO");
    }

/*
    "networkId": "UI";
    "startDate": "2006-09-11T00:00:00.000";
    "endDate": "2006-09-11T00:01:45.000";
*/
    @POST
    @Path("station/all")
    @Produces(value = MediaType.APPLICATION_JSON)
    public ArrayList<UStation> doGetMetaData(NetworkConfig obj) throws ParseException, NoDataFoundException, CriteriaException, Exception {
        ArrayList<UStation> ustations = new ArrayList();
                
        StationService stationService = serviceUtil.getStationService();
        StationCriteria stationCriteria = new StationCriteria();

        // define the dates of interest</span>
        DateFormat dfm = new SimpleDateFormat("yyyy-MM-dd");
        dfm.setTimeZone(TimeZone.getTimeZone("GMT"));

        // define the dates of interest</span>
        Date startDate = dfm.parse(obj.getStartDate());
        Date endDate = dfm.parse(obj.getEndDate());

        // specify the search criteria</span>
        stationCriteria = stationCriteria.addNetwork(obj.getNetworkId()).
        setStartAfter(startDate).setEndBefore(endDate);

        List<Network> ls = stationService.fetch(stationCriteria, OutputLevel.CHANNEL);
        for (Network n : ls) {
                //System.out.printf("Network : %s with %d stations\n", n.getCode(), n.getSelectedNumberStations());
                // each network may have multiple stations
                for (Station s : n.getStations()) {
                        UStation us = new UStation();
                        us.setStationCode(s.getCode());
                        us.setStationLat(s.getLatitude().getValue());
                        us.setStationLon(s.getLongitude().getValue());
                        us.setStationDesc(s.getDescription());
                        us.setStationStartDate(s.getStartDate() + "");
                        us.setStationEndDate(s.getEndDate() + "");
                        
                        ArrayList<UChannel> uchs = new ArrayList();
                        
                        //System.out.printf("\tStation : %s with %d channels\n", s.getCode(), s.getChannels().size());
                        //System.out.printf("\t Lat: %s, Long: %s", s.getLatitude().getValue(), s.getLongitude().getValue());
                        for (Channel c : s.getChannels()) {
                                // Finally! Print the detail
                                UChannel usch = new UChannel();
                                usch.setCode(c.getCode());
                                usch.setDateOff(c.getEndDate() + "");
                                usch.setDateOn(c.getStartDate() + "");
                                
                                uchs.add(usch);
                        }
                        
                        ustations.add(us);
                }
        }
        
        return ustations;
    }

/*
  {
      "maxLatitude": 19.0,
      "minLongitude": 17.0,
      "minMagnitude": 5.0
  }
*/
    @POST
    @Path("event/all")
    @Produces(value = MediaType.APPLICATION_JSON)
    public ArrayList<UEvent> doGetAllEvt(UEventCriteria obj) throws ParseException, NoDataFoundException, CriteriaException, Exception {
        EventService eventService = serviceUtil.getEventService();

        ArrayList<UEvent> uevts = new ArrayList();
        
        EventCriteria criteria = new EventCriteria();
        criteria.setMaximumLatitude(obj.getMaxLatitude());
        criteria.setMinimumLatitude(obj.getMinLongitude());
        criteria.setMinimumMagnitude(obj.getMinMagnitude());

        List<Event> events = eventService.fetch(criteria);

        for(Event e : events) {
           UEvent evt = new UEvent();
           
           evt.setEventType(e.getType());
           evt.setEventOrigin(e.getPreferredOrigin() + "");
           evt.setPreferedMagnitude(e.getPreferredMagnitude().getValue()+ "");
           evt.setEventRegionName(e.getFlinnEngdahlRegionName());
 
           ArrayList<UMagnitude> magList = new ArrayList();
           for(Magnitude magnitude:e.getMagnitudes()){ 
               UMagnitude um = new UMagnitude(); //MB magnitude de volume, MW magnitude de 
               um.setMagValue(magnitude.getValue()); 
               um.setMagType(e.getType());
               
               magList.add(um);
            }
           evt.setMagList(magList);
           
           uevts.add(evt);
        }
        return uevts;
    }
    

  /*
  {
      "networkId": "IU",
      "stationId": "ANMO",
      "waveLoc": "00",
      "channelId": "BHZ",
      "startDate": "2006-09-11T00:00:00",
      "endDate": "2006-09-11T00:01:00"
  }
  */
    @POST
    @Path("wave/all")
    @Produces(value = MediaType.APPLICATION_JSON)
    public ArrayList<Integer> doGetAllWave(Wave obj) throws ParseException, NoDataFoundException, CriteriaException, Exception {
        WaveformService waveformService = serviceUtil.getWaveformService();

        ArrayList<Integer> list = new ArrayList();
        // We're specifying the BulkDataRequest equivalent of:
        // IU ANMO 00 BHZ 2006-09-11T00:00:00 2006-09-11T00:01:00
        // define the dates of interest
        DateFormat dfm = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        dfm.setTimeZone(TimeZone.getTimeZone("GMT"));
        Date startDate = dfm.parse(obj.getStartDate()); //"2006-09-11T00:00:00"
        Date endDate = dfm.parse(obj.getEndDate()); //"2006-09-11T00:01:00"
        WaveformCriteria criteria = new WaveformCriteria();
        criteria.add(obj.getNetworkId(), obj.getStationId(), obj.getWaveLoc(), obj.getChannelId(), startDate, endDate); //"IU", "ANMO", "00", "BHZ", startDate, endDate    
        
        List<Timeseries> timeSeriesCollection = waveformService.fetch(criteria);

        for(Timeseries timeseries:timeSeriesCollection){
               System.out.println(timeseries.getNetworkCode() + "-" +
               timeseries.getStationCode() + " (" + timeseries.getChannelCode() + "), loc:" +
               timeseries.getLocation());
               for(Segment segment:timeseries.getSegments()){
                   list = (ArrayList)segment.getIntData();
                  //System.out.printf("Segment:\n");
                  //System.out.printf("  Start: %s", segment.getStartTime());
                  //System.out.printf("  %d samples exist in this segment\n",
                  //   segment.getSampleCount());         
            }
        }
        
        return list;
    }
}
